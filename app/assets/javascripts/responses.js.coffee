# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

root = exports ? this
root.mv_response_action = (request_id, response_id, action, section_id) ->
  $.post("requests/" + request_id + "/responses/" + response_id,
          {
            request_action:action,
            _method:"put"
          },
          (data,status) ->
            #alert("Data: " + data + "\nStatus: " + status);
            $(section_id).load('/request/' + request_id);
          );
          