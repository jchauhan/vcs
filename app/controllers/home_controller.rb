class HomeController < ApplicationController
  before_filter :authenticate_user!, :except => [:index]

  def index
    if user_signed_in?
      redirect_to '/console'
    else
      render :layout => 'home_index'
    end
  end

  def console
    @user = User.as_dashboard_dto(current_user)
  end
  
end
