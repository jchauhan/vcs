class Request::FbVouchController < ApplicationController
  before_filter :authenticate_user!
  def index
  end

  def show
  end

  def new
    params = {
      :app_id => ::MvVcs::StaticConfig.get(:facebook_app_id),
      :message => 'Do you trust me. If yes, Vouch for me by accepting this request',
      :redirect_uri => ::MvVcs::StaticConfig.get(:facebook_vouch_redirect_url)
    }
    
    redirect_to  ::MvVcs::StaticConfig.get(:facebook_apprequests_url) + "?#{params.to_query}"
  end
    
  def callback
    #TODO need to validate HTTP request GET or POst params

    message = 'Do you trust me. If yes, Vouch for me by accepting this request'
    VouchRequest.create_fb(current_user, params[:request], params[:to], message)
  end
  
  def edit
  end

  def create
  end

  def update
  end
end
