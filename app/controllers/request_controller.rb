  class RequestController < ApplicationController
    before_filter :authenticate_user!
    layout "ajaxcall"

    #
    # All the request ordered by updated time. The requests can be incoming or outgoing
    #
    def index
    @requests = Request.as_dto_arr(current_user, current_user.all_requests)
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @requests }
      end
    end
    
    
  # GET /requests/1
  # GET /requests/1.json
  def show
    @request = Request.as_dto(current_user, Request.find(params[:id]))

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @template }
    end
  end
    
    #
    # All the Requests that I have sent to friends
    #
    #
    def myrequests
       @requests = Request.as_dto_arr(current_user, current_user.myrequests)
      respond_to do |format|
        format.html # myrequests.html.erb
        format.json { render json: @requests }
      end
    end
    
    #
    # All the requests that friends has sent to me
    #
    #
    def othersrequests
       @requests = Request.as_dto_arr(current_user, current_user.others_requests)
      respond_to do |format|
        format.html # othersrequests.html.erb
        format.json { render json: @requests }
      end
    end
    
  end