class ResponsesController < ApplicationController
  layout "ajaxcall"

  def index
  end

  def show
  end

  def update
    Response.handle_action(current_user, params[:id], params[:request_action].to_sym)
     respond_to do |format|
        format.html # myrequests.html.erb
        format.json { render json: "" }
      end
  end
end
