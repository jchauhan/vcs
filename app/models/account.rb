class Account < ActiveRecord::Base
  attr_accessible :name, :type
  has_many :users, :inverse_of => :account

  validates :name,  :presence => true, :length => {:maximum => 255}
  validates :type, :presence => true, :length => { :maximum => 80 }
end
