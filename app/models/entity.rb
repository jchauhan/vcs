class Entity < ActiveRecord::Base
  attr_accessible :ex_eid, :type
  
  has_many :requests, :inverse_of => :entity
  
  belongs_to :account, :inverse_of => :entities
  validates_presence_of :account
  
  validates :ex_eid,  :presence => true, :length => {:maximum => 255}
  validates :type, :presence => true, :length => { :maximum => 80 }
end
