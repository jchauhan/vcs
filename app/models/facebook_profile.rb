class FacebookProfile < SocialProfile
  
  def self.find_by_uids(uids)
    FacebookProfile.where("uid in (?)", uids)
  end
end