class Request < ActiveRecord::Base
  attr_accessible :type, :message

  validates :type, :presence => true, :length => { :maximum => 80 }
  validates :message, :presence => true, :length => { :maximum => 1024 }


  #associations
  has_many :request_channels, :inverse_of => :request
  belongs_to :requester, :inverse_of => :myrequests, :class_name => :User
  validates_presence_of :requester

  belongs_to :entity, :inverse_of => :requests

  has_many :responses, :inverse_of => :request
  has_many :respondents, :through => :responses,  :source => :user

  has_one :stat, :inverse_of => :request, :class_name => :RequestStat

  #
  # Maps a request to User as respondents, the one who need to respond to a request from a user
  # Useful when user is signing in for the first time.
  # Params:
  # user: user model
  # ex_rcid: external Request ID, in case of facebook it will be facebook request id
  #
  def self.map_as_respondent(user, ex_rcid=nil)
    fp = user.facebook_profile
    if(ex_rcid.nil?)
      self.map_as_respondent_via_facebook(user, fp);
    else 
      self.map_as_respondent_via_facebook(user, fp, ex_rcid);
   end
  end

  def self.as_dto(current_user, req)
    #if the requester is actually the user who logged in
    if req.requester.id == current_user.id
      Dto::MyRequestDto.new(req)
    else
      Dto::OthersRequestDto.new(req, current_user)
    end
  end
  
  def self.as_dto_arr(current_user, req_arr)
    dto_arr = []
    req_arr.each{ |a|
      dto_arr << self.as_dto(current_user, a)
    }
    
    dto_arr
  end
  
  def self.compute_and_update_stat(request, activity=true)
    result = self.compute_stat(request)
    if activity
      result[:last_activity_at] = Time.now.utc
    end
    request.stat.update_attributes!(result)
  end
  
  def self.compute_and_create_stat(request)
    result = self.compute_stat(request)
    result[:last_activity_at] = Time.now.utc
    stat = RequestStat.new(result)
    stat.request = request
    stat.save!
  end
  
  def self.compute_stat(request)
    result = {}
    result[:total_req] = Request.joins(request_channels: :request_recipients).where('id' => request.id).count
    result[:accepted_req] = Request.joins(:responses).where('id' => request.id, 'responses.status' => Response::APPROVED).count
    result[:rejected_req] = Request.joins(:responses).where('id' => request.id, 'responses.status' => Response::REJECTED).count
    result[:unconfirmed_req] = result[:total_req] - result[:accepted_req] - result[:rejected_req]
    
    result
  end
  
  protected
  
  def self.map_as_respondent_via_facebook(user, fp, ex_rcid=nil)
    if(ex_rcid.nil?)
      fcs = FacebookChannel.joins(:request_recipients).where(:request_recipients => {:ex_rid => fp.uid})
    else
      fcs = FacebookChannel.joins(:request_recipients).where(:request_recipients => {:ex_rid => fp.uid}, :ex_rcid => ex_rcid)
    end
    fcs.each {|fc|
      res  = Response.new(:status => -1)
      res.user = user;
      res.request = fc.request;
      res.save!
    }
  end

end
