class RequestChannel < ActiveRecord::Base
  attr_accessible :ex_rcid, :type
  
  #TODO make both ex_rcid and type as composite key
  
  #Validations
  validates :ex_rcid,  :presence => true, :length => {:maximum => 255}
  validates :type, :presence => true, :length => { :maximum => 80 }
  
  #Associations
  has_many :request_recipients, :inverse_of => :request_channel  
  belongs_to :request, :inverse_of => :request_channels
  validates_presence_of :request
end
