class RequestRecipient < ActiveRecord::Base
  attr_accessible :ex_rid
  
  validates :ex_rid,  :presence => true, :length => {:maximum => 255}
  
  #Associations with other models
  belongs_to :request_channel, :inverse_of => :request_recipients
  validates_presence_of :request_channel
end
