class RequestStat < ActiveRecord::Base
  attr_accessible :accepted_req, :last_activity_at, :rejected_req, :total_req, :unconfirmed_req

  belongs_to :request, :inverse_of => :stat
  validates_presence_of :request

end

