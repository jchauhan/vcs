class Response < ActiveRecord::Base
  #
  attr_accessible :status

  belongs_to :user, :inverse_of => :responses
  belongs_to :request, :inverse_of => :responses
  validates_presence_of :user
  validates_presence_of :request

  APPROVED = 1
  REJECTED = 0
  UNCONFIRMED = -1

  validates :status, :numericality => { :only_integer => true, :greater_than => -2,   :less_than => 2}

  def status_to_s
    case status
    when REJECTED
      'Rejected'
    when APPROVED
      'Accepted'
    else
      'Unconfirmed'
    end
  end

  def self.handle_action(current_user, response_id, action)
    ActiveRecord::Base.transaction do
      response= Response.where("id = ? AND user_id=?",
      response_id, current_user.id).first
      if !response.nil?
        if(response.status == UNCONFIRMED)
          handle_unconfirmed_status(response, action)
        else
          handle_confirmed_status(response, action)
        end
        
        Request.compute_and_update_stat(response.request)
        
      else
        raise Exceptions::IllegalResourceAction.new "Illegal Action on the resource"
      end
    end #ActiveRecord::Base.transaction do
  end

  protected

  def self.handle_unconfirmed_status(response, action)
    case action
    when :approve
      handle_approve_action(response)
    when :reject
      handle_reject_action(response)
    else
      raise Exceptions::IllegalArguementException.new "Illegal Resource Action Arguement: " + action.to_s
    end
  end

  def self.handle_confirmed_status(response, action)
    case action
    when :revert
      handle_revert_action(response)
    else
      raise Exceptions::IllegalArguementException.new "Illegal Resource Action Arguement: " + action.to_s
    end
  end

  def self.handle_approve_action(response)
    response.status = APPROVED
    response.save!
  end

  def self.handle_reject_action(response)
    response.status = REJECTED
    response.save!
  end

  def self.handle_revert_action(response)
    response.status = UNCONFIRMED
    response.save!
  end

end
