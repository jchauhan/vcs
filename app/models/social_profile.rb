class SocialProfile < ActiveRecord::Base
    attr_accessible :uid, :type
    
    belongs_to :user, :inverse_of => :social_profiles
    validates_presence_of :user
  
    validates :type, :presence => true, :length => { :maximum => 80 }
    validates :uid, :presence => true, :length => { :maximum => 80 }

end
