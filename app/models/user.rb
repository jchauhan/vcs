class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
       :recoverable, :rememberable, :trackable, :validatable, :confirmable,
       :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :name
  # attr_accessible :title, :body
  attr_accessible :provider

  #Associations with other models
  belongs_to :account, :inverse_of => :users
  validates_presence_of :account
  
  has_many :responses, :inverse_of => :user
  has_many :others_requests, :through => :responses, :source => :request 
  
  has_many :myrequests, :inverse_of => :requester, :foreign_key => :requester_id, :class_name => :Request

  has_many :social_profiles, :inverse_of => :user

  
  #Methods
  def self.find_or_create_for_facebook_oauth(auth, signed_in_resource=nil)
    fp_profile = FacebookProfile.where(:uid => auth.uid).first;

    if fp_profile.nil?      
      user = signed_in_resource || User.create({
        :name => auth.extra.raw_info.name,
        :provider => auth.provider,
        :email => auth.info.email,
        :password => Devise.friendly_token[0,20]
        })
        
      #Create Account and associate with User
      account = EndUserAccount.create({:name => auth.extra.raw_info.name})
      account.save!
      user.account = account
      
      #Create Facebook profile and associate with User
      fp_profile = FacebookProfile.create({:uid => auth.uid}) 
      fp_profile.app_access_token = auth.credentials.token
      fp_profile.user= user
      fp_profile.save!
      
      #Map all the related pending requests to the current user
      Request.map_as_respondent(user)
      
      user.confirm!   # Auto Confirm when facebook signup
    else
      #Update Facebook access token as token can expire
      fp_profile.app_access_token = auth.credentials.token
      fp_profile.save!
    end
  
    fp_profile.user
  end
  
  #
  # Returns all requests related to the current user model. Includes myrequests + othersrequests
  #
  #
  def all_requests
    myrequests + others_requests
  end
  
  #
  #Returns:
  # true: if user registered via email
  # false: otherwise
  #
  def external_signup?
    !self.provider.nil?
  end
  
  #misc helper methods
  def facebook_profile
    self.social_profiles.where({:type => FacebookProfile.to_s}).first
  end
  
  def self.as_dashboard_dto(user)
    Dto::DashboardUserSummaryDto.new user
  end
  
end
