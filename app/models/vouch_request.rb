class VouchRequest < Request

  #
  #Create Vouch Request via Facebook Channel
  #Params
  # requester: The user show is requesting the vouching
  # request_id: request_id returned by facebook
  # recipients: Recipients who will receive the vouching request. array of facebook user ids
  #
  def self.create_fb(requester, request_id, recipients, message="")
    ActiveRecord::Base.transaction do
      fc = FacebookChannel.where(:ex_rcid => request_id).first
      request = nil
      if(fc.nil?)
        request = VouchRequest.new(:message => message)
        request.requester = requester
        request.save!

        fc = FacebookChannel.new(:ex_rcid => request_id)
        fc.request = request
        fc.save!

        recipients.each_value{|rid|
          rr = RequestRecipient.new(:ex_rid => rid)
          rr.request_channel = fc
          rr.save!
        }
        
        #Map all the related pending requests to the current user
        fps = FacebookProfile.find_by_uids(recipients.values)
        fps.each{|fp|
          Request.map_as_respondent(fp.user, request_id) 
        }
        
        Request.compute_and_create_stat(request)
        
      else
        request = fc.request
      end
      request
    end #ActiveRecord::Base.transaction do
  end

end