class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :type, :null => false, :length => 80
      t.string :name, :null => false, :length => 255

      t.timestamps
    end
    
    add_index :accounts, :type
    add_index :accounts, :name
  end
  
  def down
    drop_table :accounts
  end
end
