class AddAccountForiegnKeyToUser < ActiveRecord::Migration  
  def change
    change_table(:users) do |t|
      t.belongs_to :account
   end
    add_index :users, :account_id
  end
  
  def down
    t.remove_belongs_to :account
  end
end
