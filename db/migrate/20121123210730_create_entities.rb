class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.string :type
      t.string :ex_eid
      t.belongs_to :account

      t.timestamps
    end
    
    add_index :entities, :type
   add_index :entities, :ex_eid
   add_index :entities, :account_id
  end
end
