class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :type
      t.string :message, :length => 1024
      t.belongs_to :requester, :class_name => :User
      t.belongs_to :entity
      
      t.timestamps
    end
    
    add_index :requests, :type
    add_index :requests, :requester_id
    add_index :requests, :entity_id
  end
end
