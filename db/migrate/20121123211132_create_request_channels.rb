class CreateRequestChannels < ActiveRecord::Migration
  def change
    create_table :request_channels do |t|
      t.string :type
      t.string :ex_rcid
      t.belongs_to :request

      t.timestamps
    end
    
    add_index :request_channels, :type
    add_index :request_channels, :ex_rcid
    add_index :request_channels, :request_id
    
  end
end
