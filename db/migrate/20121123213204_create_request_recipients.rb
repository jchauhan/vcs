class CreateRequestRecipients < ActiveRecord::Migration
  def change
    create_table :request_recipients do |t|
      t.string :ex_rid
      t.belongs_to :request_channel
      
      t.timestamps
    end
   add_index :request_recipients, :ex_rid
   add_index :request_recipients, :request_channel_id
  end
  
end
