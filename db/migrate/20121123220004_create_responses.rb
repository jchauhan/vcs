class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.integer :status
      t.belongs_to :user
      t.belongs_to :request
      t.timestamps
    end
    
    add_index :responses, :request_id
    add_index :responses, :user_id    
  end
end
