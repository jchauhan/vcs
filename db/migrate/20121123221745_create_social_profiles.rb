class CreateSocialProfiles < ActiveRecord::Migration
  def change
    create_table :social_profiles do |t|
      t.string :type
      t.string :uid
      t.string :app_access_token, :limit => 200

      t.belongs_to :user

      t.timestamps
    end
    
    add_index :social_profiles, :user_id   
    add_index :social_profiles, :type
    add_index :social_profiles, :uid
  end
end
