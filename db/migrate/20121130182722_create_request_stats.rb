class CreateRequestStats < ActiveRecord::Migration
  def change
    create_table :request_stats do |t|
      t.integer :total_req
      t.integer :accepted_req
      t.integer :rejected_req
      t.integer :unconfirmed_req
      t.datetime :last_activity_at

      t.belongs_to :request
      
      t.timestamps
    end
    
    add_index :request_stats, :request_id   

  end
end
