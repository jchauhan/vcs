class CreateRequestComments < ActiveRecord::Migration
  def change
    create_table :request_comments do |t|
      t.string :message

      t.belongs_to :request
      t.belongs_to :user
      
      t.timestamps
    end
    
    add_index :request_comments, :request_id   
    add_index :request_comments, :user_id   
    
  end
end
