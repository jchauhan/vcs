# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121130182833) do

  create_table "accounts", :force => true do |t|
    t.string   "type",       :null => false
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "accounts", ["name"], :name => "index_accounts_on_name"
  add_index "accounts", ["type"], :name => "index_accounts_on_type"

  create_table "entities", :force => true do |t|
    t.string   "type"
    t.string   "ex_eid"
    t.integer  "account_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "entities", ["account_id"], :name => "index_entities_on_account_id"
  add_index "entities", ["ex_eid"], :name => "index_entities_on_ex_eid"
  add_index "entities", ["type"], :name => "index_entities_on_type"

  create_table "request_channels", :force => true do |t|
    t.string   "type"
    t.string   "ex_rcid"
    t.integer  "request_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "request_channels", ["ex_rcid"], :name => "index_request_channels_on_ex_rcid"
  add_index "request_channels", ["request_id"], :name => "index_request_channels_on_request_id"
  add_index "request_channels", ["type"], :name => "index_request_channels_on_type"

  create_table "request_comments", :force => true do |t|
    t.string   "message"
    t.integer  "request_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "request_comments", ["request_id"], :name => "index_request_comments_on_request_id"
  add_index "request_comments", ["user_id"], :name => "index_request_comments_on_user_id"

  create_table "request_recipients", :force => true do |t|
    t.string   "ex_rid"
    t.integer  "request_channel_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "request_recipients", ["ex_rid"], :name => "index_request_recipients_on_ex_rid"
  add_index "request_recipients", ["request_channel_id"], :name => "index_request_recipients_on_request_channel_id"

  create_table "request_stats", :force => true do |t|
    t.integer  "total_req"
    t.integer  "accepted_req"
    t.integer  "rejected_req"
    t.integer  "unconfirmed_req"
    t.datetime "last_activity_at"
    t.integer  "request_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "request_stats", ["request_id"], :name => "index_request_stats_on_request_id"

  create_table "requests", :force => true do |t|
    t.string   "type"
    t.string   "message"
    t.integer  "requester_id"
    t.integer  "entity_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "requests", ["entity_id"], :name => "index_requests_on_entity_id"
  add_index "requests", ["requester_id"], :name => "index_requests_on_requester_id"
  add_index "requests", ["type"], :name => "index_requests_on_type"

  create_table "responses", :force => true do |t|
    t.integer  "status"
    t.integer  "user_id"
    t.integer  "request_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "responses", ["request_id"], :name => "index_responses_on_request_id"
  add_index "responses", ["user_id"], :name => "index_responses_on_user_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "social_profiles", :force => true do |t|
    t.string   "type"
    t.string   "uid"
    t.string   "app_access_token", :limit => 200
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "social_profiles", ["type"], :name => "index_social_profiles_on_type"
  add_index "social_profiles", ["uid"], :name => "index_social_profiles_on_uid"
  add_index "social_profiles", ["user_id"], :name => "index_social_profiles_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "authentication_token"
    t.string   "name"
    t.string   "provider"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "account_id"
  end

  add_index "users", ["account_id"], :name => "index_users_on_account_id"
  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
