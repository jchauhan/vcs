class Dto::BaseUserSummaryDto

  include ActiveModel::Serialization

  attr_accessor :user_name, :sp_uid
  attr_accessor :has_fb_profile

  def initialize(user)
    @user_name = user.name
    fp = user.facebook_profile
    if(!fp.nil?)
      @has_fb_profile = true
      @sp_uid = fp.uid
    end
  end

  def has_fb_profile?
    return @has_fb_profile
  end
end