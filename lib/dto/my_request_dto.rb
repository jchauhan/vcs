class Dto::MyRequestDto < Dto::RequestDto
  
  
  def initialize(request)
    super
  end
  
  def my_request?
    return true
  end
  
  def others_request?
    return false
  end
end