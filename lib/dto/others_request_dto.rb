class Dto::OthersRequestDto < Dto::RequestDto
    attr_accessor :response

  def initialize(request, current_user)
    super(request)
     @response = Response.joins(:user, :request).where(:user_id => current_user.id, 
          :request_id => request.id).first
  end
  
  def my_request?
    return false
  end
  
  def others_request?
    return true
  end
  
  def unconfirmed?
    response.status == Response::UNCONFIRMED
  end
  
end