class Dto::RequestDto

  include ActiveModel::Serialization

  attr_accessor :user_name, :sp_uid
  attr_accessor :request
  attr_accessor :stat


  def initialize(request)
    user = request.requester    
    @request = request
    @stat = request.stat
    @user_name = user.name
    fp = user.facebook_profile
    if(!fp.nil?)
      @sp_uid = fp.uid
    end
  end

  def facebook_profile?
    return true
  end
end