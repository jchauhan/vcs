#!/bin/bash

PID=`ps aux | grep rails | grep 5700 | awk {'print $2'}`
if [ -z "$PID" ]; then
        echo "** Failed to find PID of rails server"
        #exit
else
        for xpid in $PID; do
                kill -9 $xpid
        done;
fi;

echo "** Starting rails"
rails s -b 127.0.0.1 -p 57001 -e production  -d
rails s -b 127.0.0.1 -p 57002 -e production  -d

